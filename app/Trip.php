<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
    	'title',
    	'price',
    	'price_discount',
    	'image',
    	'description'
    ];
}
