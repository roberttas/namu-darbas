<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// Route::get('/admin', function () {
//     return view('trips.index');
// });


// Route::resource('/trips', 'TripController');

Route::resource('/trips', 'TripController@index');

Route::get('/create', 'TripController@create');
Route::resource('trips', 'TripController');
Route::resource('user', 'UserController');




// <div class="row">
//     @foreach($trips as $index => $trip)
//         <div class="col-md-4">
//             <div class="l3photo-background">
//                 <img class="l3photo" src="{{ $trip->image }}" height="200" width="100%">
//                 @if (Auth::guest())
//                 @else
//                 <span class="l3price"><strike>{{ $trip->price }} €</strike> <strong>{{ $trip->price_discount }} €</strong></span>
//                 @endif
//                 <h3>{{ $trip->title }}</h3>
//                 <p>{{ $trip->description }}</p>
//             </div>
//             <br>
//         </div>
//         @if(++$index % 3 == 0)
//         </div><div class="row">
//         @endif
//     @endforeach
// </div>