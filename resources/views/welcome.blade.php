{{  Form::hidden ($trips = \App\Trip::all()) }}
@extends('layout.main')

@section('content')
    <div class="container">
    <!-- sliderio idejimas -->
    
        <div class="slider">
          <img class="mySlides" src="../public/img/Sydney.jpg">
          <img class="mySlides" src="../public/img/seoul.jpg">
          <img class="mySlides" src="../public/img/london.jpg">
          <img class="mySlides" src="../public/img/paris.jpg">
          <img class="mySlides" src="../public/img/italy.jpg">
          <img class="mySlides" src="../public/img/venice.jpg">
        </div>
        
        <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
               x[i].style.display = "none";
            }
            myIndex++;
            if (myIndex > x.length) {myIndex = 1}
            x[myIndex-1].style.display = "block";
            setTimeout(carousel, 3000); // Change image every 3 seconds
        }
        </script>
    <!-- sliderio pabaiga -->
        <div class="layer1">
            <h3><b>Ei keliautojau!</b></h3>
            <span>Pradėkite kelionę planuotis <br/>jau dabar!</span>
        </div>
        <div id="keliones" class="layer2">
            <div>
                <h1>Kelionės</h1>
            </div>
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="l2photo-background overlay">
                        <img class="l2photo" src="../public/img/statue.jpg">
                        <h4>Šiaurės<br/> Amerika</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="l2photo-background">
                        <img class="l2photo" src="../public/img/paris2.jpg">
                        <h4>Europa</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="l2photo-background overlay">
                        <img class="l2photo" src="../public/img/asia.jpg">
                        <h4>Azija</h4>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="l2photo-background">
                        <img class="l2photo" src="../public/img/sopera.jpg">
                        <h4>Australija</h4>
                    </div>
                </div>
            </div>
        </div>
        <div id="pasiulymai" class="layer3">
            <div>
                <h1>Pasiūlymai</h1>
            </div>
            
                <div class="row">
                    @foreach($trips as $index => $trip)
                        <div class="col-md-4">
                            <div class="l3photo-background">
                                <img class="l3photo" src="{{ $trip->image }}" height="200" width="100%">
                                @if (Auth::guest())
                                @else
                                <span class="l3price"><strike>{{ $trip->price }} €</strike> <strong>{{ $trip->price_discount }} €</strong></span>
                                @endif
                                <h3>{{ $trip->title }}</h3>
                                <p>{{ $trip->description }}</p>
                            </div>
                            <br>
                        </div>
                        @if(++$index % 3 == 0)
                            </div><div class="row">
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>        
@endsection

