
@extends("layout.main")
@section("content")
<div class="forms">
    <h3>{{ isset($user) ? 'Edit user' : 'Add user' }}</h3>
    @if(isset($user))
        {{ Form::open(['route' => ['user.update', $user->id], 'method' => "POST"]) }}
        {{ Form::hidden('_method', 'PUT') }}
    @else
        {{ Form::open(['route' => 'user.store', 'method' => "POST"]) }}
    @endif
    {{ csrf_field() }}
        {{ Form::label('name') }}
        @if(isset($user))
            {{ Form::text('name', ($user['name']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('name', '', ['class' => 'form-control']) }}
        @endif
        {{ Form::label('Email') }}
        @if(isset($user))
            {{ Form::text('email', ($user['email']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('email', '', ['class' => 'form-control']) }}
        @endif
        {{ Form::label('password') }}
        {{ Form::text('password', '', ['class' => 'form-control']) }}
        {{ Form::label('confirm password') }}
        {{ Form::text('confirm_password', '', ['class' => 'form-control']) }}
        {{ Form::label('') }}
        @if(isset($user))
            {{ Form::label('is_admin', "Is admin?") }}
            {{ Form::hidden("is_admin", 0) }}
            {{ Form::checkbox('is_admin', 1, $user->is_admin) }}
        @else
            {{ Form::label('is_admin', "Is admin?") }}
            {{ Form::hidden("is_admin", 0) }}
            {{ Form::checkbox('is_admin', 1, 0) }}
        @endif
        <br>



    @if(isset($user))
        {{ Form::submit('Edit user', ['class' => 'btn btn-primary']) }}
    @else
        {{ Form::submit('Add user', ['class' => 'btn btn-primary']) }}
    @endif
    {{ Form::close() }}
    <br>
    @if(isset($user))
        {{ Form::open(['route' => ['user.destroy', $user->id], 'method' => "POST"]) }}
        {{ Form::hidden('_method', 'DELETE') }}
                {{ csrf_field() }}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
        
    @endif
    {{ Form::close() }}
</div>
@endsection
