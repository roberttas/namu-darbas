@extends('layout.main')

@section('content')
<div class="container forms">
	<h2>VARTOTOJAI</h2>

	@foreach($users as $user)
		<div class="col-md-12">
			<ul>
				<li>User name: {{ $user->name }}</li>
				<li>User email: {{ $user->email }}</li>
				<li>Admin = 1; Simple user = 0</li>
				{{ $user->is_admin }}
				<br>
				<a href="{{ route('user.edit', $user->id) }}">EDIT user</a>
			</ul>
		</div>
	@endforeach
</div>
@endsection
