<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
	<title>traveler</title>
</head>
<body>
	<div class="container">
		<header class="headerClass navigation">
			<div class="clearfix">
				<div class="logo">
					<a href="/traveler/public">
						<img src="{{ asset('logo/Logotipas.png') }}" height="100" width="130">
					</a>
				</div>
				<div>
					<nav>
						<ul>
							@if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Prisijunkti</a></li>
                            <li><a href="{{ url('/register') }}">Registruotis</a></li>
                        	@else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                       		 @endif

                       		
                       		@if ( Auth::check() && Auth::user()->is_admin == 1 )
								<li><a href="/traveler/public/trips" title="admin">administrator</a></li>
							@else	
                       		@endif
							<li><a href="/traveler/public#kontaktai" title="Kontaktai">Kontaktai</a></li>							
							<li><a href="/traveler/public#pasiulymai" title="Pasiūlymai">Pasiūlymai</a></li>
							<li><a href="/traveler/public#keliones" title="Kelionės">Kelionės</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
	</div>

	<div class="container">
		@yield('content')
	</div>
	
	<div class="container">
		<footer id="kontaktai">
			<div class="row footeR">
				<div class="col-md-2"></div>
				<div class="col-md-2">
					<div class="">
						<h2>Kontaktai</h2>
						<p>Švitrigailos g. 8, LT-03127, Vilnius </p>
						<p>Tel.: (8 5) 888 9999</p>
						<p>El.p.: info@traveler.com</p>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="">
						<h2>Sekite mus:</h2>
						<a href="https://facebook.com" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
						<a href="https://twitter.com " target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
						<a href="https://instagram.com" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
						<a href="https://linkedin.com" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
					</div>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-2">
					<div class="">
						<h2>Mus rasite:</h2>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2306.73412073999!2d25.265054215308073!3d54.679107981748956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd9472c09e8bb1%3A0x8d2b9e6b4a1aa84f!2s%C5%A0vitrigailos+g.+8%2C+Vilnius+03223!5e0!3m2!1slt!2slt!4v1480351336497" width="270" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
				</div>
				<div class="col-md-2"></div>
			</div>
            <div class="coppyright">
            <hr>
                <h6>&#xA9; 2001-2016 UAB "traveler". Visos teisės saugomos.</h6> 
            </div>
		</footer>
	</div>	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>