@extends('layout.main')
@section("content")
<div class="forms">
    <h3>{{ isset($trip) ? 'Edit trip' : 'Add trip' }}</h3>
    @if(isset($trip))
        {{ Form::open(['route' => ['trips.update', $trip->id], 'method' => "POST"]) }}
        {{ Form::hidden('_method', 'PUT') }}
    @else
        {{ Form::open(['route' => 'trips.store', 'method' => "POST"]) }}
    @endif
    {{ csrf_field() }}

        {{ Form::label('title', 'Title')}}
        @if(isset($trip))
            {{ Form::text('title', ($trip['title']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('title', '', ['class' => 'form-control']) }}
        @endif

        {{ Form::label('price', 'Price') }}
        @if(isset($trip))
            {{ Form::text('price', ($trip['price']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('price', '', ['class' => 'form-control']) }}
        @endif

        {{ Form::label('price_discount', 'Price_discount') }}
        @if(isset($trip))
            {{ Form::text('price_discount', ($trip['price_discount']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('price_discount', '', ['class' => 'form-control']) }}
        @endif

        {{ Form::label('image', 'Image URL') }}
        @if(isset($trip))
            {{ Form::text('image', ($trip['image']), ['class' => 'form-control']) }}
        @else
            {{ Form::text('image', '', ['class' => 'form-control']) }}
        @endif

        {{ Form::label('description', 'Description') }}
        @if(isset($trip))
            {{ Form::textarea('description', ($trip['description']), ['class' => 'form-control']) }}
        @else
            {{ Form::textarea('description', '', ['class' => 'form-control']) }}
        @endif
        <br>
    @if(isset($trip))
        {{ Form::submit('Edit trip', ['class' => 'btn btn-primary']) }}
    @else
        {{ Form::submit('Add trip', ['class' => 'btn btn-primary']) }}
    @endif

    {{ Form::close() }}
    <br>
    @if(isset($trip))
        {{ Form::open(['route' => ['trips.destroy', $trip->id], 'method' => "POST"]) }}
        {{ Form::hidden('_method', 'DELETE') }}
                {{ csrf_field() }}
            {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}    
    @endif
    {{ Form::close() }}
</div>
@endsection