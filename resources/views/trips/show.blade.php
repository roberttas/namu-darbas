@extends('layout.main')

@section('content')

<div class="row logReg">
    <div class="col-md-4">
        <div class="l3photo-background">
            <img class="l3photo" src="{{ $trip->image }}" height="200" width="100%">
            @if (Auth::guest())
            @else
            <span class="l3price"><strike>{{ $trip->price }} €</strike> <strong>{{ $trip->price_discount }} €</strong></span>
            @endif
            <h3>{{ $trip->title }}</h3>
            <p>{{ $trip->description }}</p>
        </div>
    </div>

@endsection