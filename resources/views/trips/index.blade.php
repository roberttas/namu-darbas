
@extends('layout.main')
@section ('content')
    <div class="forms">
        <div id="" class="layer3">
            <div>
                <h1>Pasiūlymai</h1>
                <a class="btn btn-warning" href="/traveler/public/user">Users menus</a>
                <br>
                <br>
                <a class="btn btn-warning" href="/traveler/public/create">Create trip</a>
            </div>
            <br>
                <div class="row">
                    @foreach($trips as $index => $trip)
                        <div class="col-md-4">
                            <div class="l3photo-background">
                                <img class="l3photo" src="{{ $trip->image }}" height="200" width="100%">
                                @if (Auth::guest())
                                @else
                                <span class="l3price"><strike>{{ $trip->price }} €</strike> <strong>{{ $trip->price_discount }} €</strong></span>
                                @endif
                                <h3>{{ $trip->title }}</h3>
                                <p>{{ $trip->description }}</p>
                            </div>
                            <div class="indexx">
                                <a class="btn btn-warning" href="{{ route('trips.edit', $trip->id) }}">EDIT</a>
                                <a class="btn btn-warning" href="{{ route('trips.show', $trip->id) }}">READ MORE</a>
                            </div>
                            <br>
                        </div>
                        @if(++$index % 3 == 0)
                        </div><div class="row">
                        @endif
                    @endforeach
                </div>
            <br>
        </div>
    </div>
@endsection